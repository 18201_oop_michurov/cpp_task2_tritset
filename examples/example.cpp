#include <iostream>


#include "../src/tritset.hpp"


int main()
{
    TritSet my_awesome_set = {
            Trit::False,
            Trit::Unknown,
            Trit::True
    };

    TritSet even_greater_set = {
            Trit::True,
            Trit::True,
            Trit::True
    };

    TritSet long_set = my_awesome_set + even_greater_set >> 9;

    std::cout << "There are: " << std::endl;

    for (auto const & pair : long_set.cardinality())
    {
        std::cout << "\t" << pair.second << " trits with value " << pair.first  << std::endl;
    }

    std::cout << "in long_set" << std::endl;

    std::cout << long_set << std::endl;

    TritSet another_long_set = long_set;

    another_long_set <<= 1;

    long_set &= ~another_long_set;

    std::cout << long_set << std::endl;

    for (auto & trit : long_set)
    {
        trit = ~trit;
    }

    std::cout << long_set << std::endl;

    long_set >>= long_set.length() - 5;

    long_set.trim(long_set.length() - 2);

    long_set[100] = Trit::True;

    std::cout << long_set << std::endl;

    long_set[100] = Trit::Unknown;

    size_t previous_capacity = long_set.capacity();

    long_set.shrink();

    if (long_set.capacity() < previous_capacity)
    {
        std::cout << "Unused memory is freed" << std::endl;
    }

    std::cout << long_set << std::endl;

    for (size_t i = long_set.length(); i > 0; --i)
    {
        long_set <<= 1;

        std::cout << long_set << std::endl;
    }

    if (long_set.empty())
    {
        std::cout << "long_set is empty now" << std::endl;
    }

    previous_capacity = long_set.capacity();

    long_set >>= 100;

    if (long_set.empty() and previous_capacity == long_set.capacity())
    {
        std::cout << "long_set is still empty now and no memory was allocated" << std::endl;
    }

    return 0;
}