# TritSet

A container class that effectively manages memeory when storing sequences of trits.

## Usage
[TritSet member functions](https://gitlab.com/18201_oop_michurov/cpp_task2_tritset/blob/master/src/tritset.hpp#L73)

[TritSet non-member functions](https://gitlab.com/18201_oop_michurov/cpp_task2_tritset/blob/master/src/tritset.hpp#L236)

[Usage example](https://gitlab.com/18201_oop_michurov/cpp_task2_tritset/blob/master/examples/example.cpp)
