#define CATCH_CONFIG_MAIN


#include <utility>
#include "../catch.hpp"


#include "../src/tritset.hpp"


TEST_CASE("test constructors")
{
    SECTION("test default constructor")
    {
        TritSet set;

        REQUIRE(set.capacity() == 0);
        REQUIRE(set.get_data_array_size() == 0);
        REQUIRE(set.get_data_pointer() == nullptr);
        REQUIRE(set.length() == 0);
    }

    SECTION("test capacity constructor")
    {
        SECTION("zero capacity")
        {
            TritSet set(0u);

            REQUIRE(set.capacity() == 0);
            REQUIRE(set.get_data_array_size() == 0);
            REQUIRE(set.length() == 0);

            TritSet set_b(0u, Trit::False);

            REQUIRE(set_b.capacity() == 0);
            REQUIRE(set_b.get_data_array_size() == 0);
            REQUIRE(set_b.length() == 0);
        }

        TritSet set(50);

        REQUIRE(set.capacity() >= 50);
        REQUIRE(set.get_data_array_size() == ((50 * 2) + sizeof(unsigned int) * 8 - 1) / 8 / sizeof(unsigned int));
        REQUIRE(set.get_data_pointer() != nullptr);
        REQUIRE(set.length() == 0);

        SECTION("filler value is not Unknown")
        {
            TritSet set_b(50, Trit::False);

            REQUIRE(set_b.capacity() >= 50);
            REQUIRE(set_b.get_data_array_size() == ((50 * 2) + sizeof(unsigned int) * 8 - 1) / 8 / sizeof(unsigned int));
            REQUIRE(set_b.length() == 50);

            bool match = true;

            TritSet const & const_set_b = set_b;

            for (auto const trit : const_set_b)
            {
                if (not (match = trit == Trit::False))
                {
                    break;
                }
            }

            REQUIRE(match);

            TritSet set_c(4 * sizeof(unsigned int), Trit::True);

            REQUIRE(set_c.capacity() == 4 * sizeof(unsigned int));
            REQUIRE(set_c.get_data_array_size() == 1);
            REQUIRE(set_c.length() == 4 * sizeof(unsigned int));

            TritSet const & const_set_c = set_c;

            for (auto const trit : const_set_c)
            {
                if (not (match = trit == Trit::True))
                {
                    break;
                }
            }

            REQUIRE(match);
        }
    }

    SECTION("test initializer list constructor")
    {
        SECTION("empty list")
        {
            TritSet set = {};

            REQUIRE(set.capacity() == 0);
            REQUIRE(set.get_data_array_size() == 0);
            REQUIRE(set.length() == 0);
        }

        SECTION("only unknown values")
        {
            TritSet set = {
                    Trit::Unknown, Trit::Unknown, Trit::Unknown,
                    Trit::Unknown, Trit::Unknown, Trit::Unknown,
                    Trit::Unknown, Trit::Unknown, Trit::Unknown,
                    Trit::Unknown, Trit::Unknown, Trit::Unknown,
                    Trit::Unknown, Trit::Unknown, Trit::Unknown,
                    Trit::Unknown, Trit::Unknown, Trit::Unknown,
                    Trit::Unknown, Trit::Unknown, Trit::Unknown,
                    Trit::Unknown, Trit::Unknown, Trit::Unknown,
                    Trit::Unknown, Trit::Unknown, Trit::Unknown,
                    Trit::Unknown, Trit::Unknown, Trit::Unknown,
                    Trit::Unknown, Trit::Unknown, Trit::Unknown,
                    Trit::Unknown, Trit::Unknown, Trit::Unknown,
                    Trit::Unknown, Trit::Unknown, Trit::Unknown,
                    Trit::Unknown, Trit::Unknown, Trit::Unknown
            };

            REQUIRE(set.capacity() == 0);
            REQUIRE(set.length() == 0);
        }

        TritSet set = {
                Trit::True, Trit::Unknown, Trit::False,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown
        };

        REQUIRE(set.capacity() == sizeof(unsigned int) * 4);
        REQUIRE(set.length() == 3);
        REQUIRE(set.get_data_array_size() == 1);

        REQUIRE(set.get(0) == Trit::True);
        REQUIRE(set.get(1) == Trit::Unknown);
        REQUIRE(set.get(2) == Trit::False);
    }

    SECTION("test copy constructor")
    {
        TritSet set_a = {
                Trit::True, Trit::Unknown, Trit::False, Trit::True, Trit::Unknown, Trit::False,
                Trit::True, Trit::Unknown, Trit::False, Trit::True, Trit::Unknown, Trit::False,
                Trit::True, Trit::Unknown, Trit::False, Trit::True, Trit::Unknown, Trit::False,
                Trit::True, Trit::Unknown, Trit::False, Trit::True, Trit::Unknown, Trit::False,
                Trit::True, Trit::Unknown, Trit::False, Trit::True, Trit::Unknown, Trit::False
        };

        TritSet set_b(set_a);

        REQUIRE(set_a.capacity() == set_b.capacity());
        REQUIRE(set_a.length() == set_b.length());
        REQUIRE(set_a.get_data_array_size() == set_b.get_data_array_size());
        REQUIRE(set_a.get_data_pointer() != set_b.get_data_pointer());
        REQUIRE(std::equal(set_a.get_data_pointer(), set_a.get_data_pointer() + set_a.get_data_array_size(),
                           set_b.get_data_pointer()));
    }
}


TritSet makeSet()
{
    TritSet set = { Trit::True, Trit::False };

    set[2] = Trit::False;

    return set;
}


TEST_CASE("test move-constructor and move-assignment operator")
{
    SECTION("move-constructor")
    {
        TritSet const set_a = makeSet();
        TritSet const set_b(makeSet());

        REQUIRE(set_a == TritSet({ Trit::True, Trit::False, Trit::False }));
        REQUIRE(set_b == TritSet({ Trit::True, Trit::False, Trit::False }));
    }

    SECTION("move-assignment")
    {
        TritSet set_a = makeSet();

        REQUIRE(set_a == TritSet({ Trit::True, Trit::False, Trit::False }));

        TritSet set_b = { Trit::True, Trit::False, Trit::True, Trit::False };

        TritSet const set_b_copy = set_b;

        set_a = std::move(set_b);

        REQUIRE(set_a == set_b_copy);
    }
}


TEST_CASE("test TritSet::to_string method and output operator<<")
{
    SECTION("TritSet")
    {
        SECTION("empty set")
        {
            TritSet set;

            std::stringstream os;

            os << set;

            REQUIRE(os.str().empty());
        }

        TritSet set = {
                Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
                Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
                Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
                Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
                Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
                Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
                Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown
        };

        std::stringstream os;

        REQUIRE(set.length() == 41u);

        REQUIRE(set.to_string() == "11?10?11?10?11?10?11?10?11?10?11?10?11?10");

        os << set;

        REQUIRE(os.str() == "11?10?11?10?11?10?11?10?11?10?11?10?11?10");
    }

    SECTION("Trit")
    {
        std::stringstream os;

        os << Trit::True << Trit::Unknown << Trit::False;

        REQUIRE(os.str() == "1?0");
    }
}


TEST_CASE("test iterators and range-based loops")
{
    TritSet set = {
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown
    };

    REQUIRE(set.capacity() == 48);
    CHECK(set.length() == 41);
    REQUIRE(set.get_data_array_size() == 3);

    std::stringstream os;

    TritSet const & const_set = set;

    for (auto const & trit : const_set)
    {
        os << trit;
    }

    REQUIRE(os.str() == "11?10?11?10?11?10?11?10?11?10?11?10?11?10");

    for (auto & trit : set)
    {
        trit = Trit::Unknown;
    }

    REQUIRE(set.length() == 0);
    REQUIRE(set.capacity() == 48);

    for (auto it = set.begin(); it != set.end(); ++it)
    {
        *it = Trit::True;
    }

    INFO("assignment never happens since Tritset::end() "
         "returns iterator pointing to last element in terms of logical length")

    REQUIRE(set.capacity() == 48);
    REQUIRE(set.length() == 0);

    REQUIRE(set.to_string().empty());
}


TEST_CASE("test TritSet::trim")
{
    TritSet set = {
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown
    };

    auto capacity = set.capacity();

    REQUIRE(set.length() == 41u);
    REQUIRE(set.to_string() == "11?10?11?10?11?10?11?10?11?10?11?10?11?10");

    set.trim(31);

    REQUIRE(set.capacity() == capacity);
    REQUIRE(set.length() == 31);
    REQUIRE(set.to_string() == "11?10?11?10?11?10?11?10?11?10?1");

    set.trim(2);

    REQUIRE(set.capacity() == capacity);
    REQUIRE(set.length() == 2);
    REQUIRE(set.to_string() == "11");

    set.trim(1000);

    REQUIRE(set.capacity() == capacity);
    REQUIRE(set.length() == 2);
    REQUIRE(set.to_string() == "11");

    SECTION("accepts zero as last_index")
    {
        set.trim(0);

        REQUIRE(set.capacity() == capacity);
        REQUIRE(set.length() == 0);
        REQUIRE(set.to_string().empty());
    }
}


TEST_CASE("test TritSet::shrink")
{
    TritSet set(1000);

    REQUIRE(set.capacity() >= 1000 * 2 / 8 / sizeof(unsigned int));

    auto initial_data_array_size = set.get_data_array_size();
    auto p = set.get_data_pointer();

    set.shrink();

    REQUIRE(set.capacity() >= 1000 * 2 / 8 / sizeof(unsigned int));
    REQUIRE(p == set.get_data_pointer());

    set.set(1500, Trit::True);
    set.set(2000, Trit::True);

    auto alloc_length = set.capacity();

    REQUIRE(set.capacity() >= 2000 * 2 / 8 / sizeof(unsigned int));

    set.set(500, Trit::False);
    REQUIRE(set.capacity() == alloc_length);

    set.set(2000, Trit::Unknown);
    REQUIRE(set.capacity() == alloc_length);

    set.shrink();

    REQUIRE(set.capacity() < alloc_length);
    REQUIRE(set.capacity() >= 1500 * 2 / 8 / sizeof(unsigned int));

    alloc_length = set.capacity();

    set.set(1500, Trit::Unknown);

    set.shrink();

    REQUIRE(set.capacity() < alloc_length);
    REQUIRE(set.get_data_array_size() == initial_data_array_size);
}


TEST_CASE("test Trit tritwise operators")
{
    SECTION("and / &")
    {
        Trit t1 = Trit::Unknown;

        t1 &= Trit::True;
        REQUIRE(t1 == Trit::Unknown);

        t1 &= Trit::False;
        REQUIRE(t1 == Trit::False);

        t1 &= Trit::True;
        REQUIRE(t1 == Trit::False);

        REQUIRE((Trit::True & Trit::Unknown) == Trit::Unknown);

        REQUIRE((Trit::True & Trit::False) == Trit::False);

        REQUIRE((Trit::Unknown & Trit::False) == Trit::False);
    }

    SECTION("or / |")
    {
        Trit t1 = Trit::Unknown;

        t1 |= Trit::True;
        REQUIRE(t1 == Trit::True);

        t1 = Trit::Unknown;
        t1 |= Trit::False;
        REQUIRE(t1 == Trit::Unknown);

        t1 = Trit::False;
        t1 |= Trit::True;
        REQUIRE(t1 == Trit::True);

        REQUIRE((Trit::True | Trit::Unknown) == Trit::True);

        REQUIRE((Trit::True | Trit::False) == Trit::True);

        REQUIRE((Trit::Unknown | Trit::False) == Trit::Unknown);
    }

    SECTION("not / ~")
    {
        REQUIRE(~Trit::True == Trit::False);

        REQUIRE(~Trit::False == Trit::True);

        REQUIRE(~Trit::Unknown == Trit::Unknown);
    }
}


TEST_CASE("test TritSet tritwise operators")
{
    SECTION("and / &")
    {
        TritSet const set_a = {
                Trit::True, Trit::False,
                Trit::True, Trit::True,
                Trit::True, Trit::True,
                Trit::True, Trit::True,
                Trit::True, Trit::True,
                Trit::True, Trit::True,
                Trit::True, Trit::True,
                Trit::True, Trit::True,
                Trit::True, Trit::True,
                Trit::True, Trit::True
        };

        TritSet set_b = { Trit::False, Trit::Unknown, Trit::False };

        REQUIRE((set_a & set_b) == std::initializer_list<Trit>({ Trit::False, Trit::False, Trit::False }));

        REQUIRE(std::initializer_list<Trit>({ Trit::False, Trit::False, Trit::False }) == (set_a & set_b));

        TritSet set_c;

        set_c &= set_a;

        TritSet set_d;

        set_d = set_c;

        REQUIRE(set_c == TritSet({ Trit::Unknown, Trit::False }));

        REQUIRE(set_d == TritSet({ Trit::Unknown, Trit::False }));

        for (auto & trit : set_b)
        {
            trit &= Trit::False;
        }

        REQUIRE(set_b == TritSet({ Trit::False, Trit::False, Trit::False }));
    }

    SECTION("or / |")
    {
        TritSet const set_a = {
                Trit::True, Trit::False,
                Trit::True, Trit::True,
                Trit::True, Trit::True,
                Trit::True, Trit::True,
                Trit::True, Trit::True,
                Trit::True, Trit::True,
                Trit::True, Trit::True,
                Trit::True, Trit::True,
                Trit::True, Trit::True,
                Trit::True, Trit::True
        };

        TritSet set_b = { Trit::False, Trit::Unknown, Trit::False };

        REQUIRE((set_a | set_b) == TritSet(
                {
                        Trit::True, Trit::Unknown,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True
                }
        ));

        TritSet set_c;

        set_c |= set_a;

        TritSet set_d;

        set_d = set_c;

        REQUIRE(set_c == TritSet(
                {
                        Trit::True, Trit::Unknown,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True
                }
        ));

        REQUIRE(set_d == TritSet(
                {
                        Trit::True, Trit::Unknown,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True,
                        Trit::True, Trit::True
                }
        ));

        for (auto & trit : set_b)
        {
            trit |= Trit::True;
        }

        REQUIRE(set_b == TritSet({ Trit::True, Trit::True, Trit::True }));
    }
    SECTION("not / ~")
    {
        TritSet set_a = { Trit::False, Trit::Unknown, Trit::True };

        REQUIRE(~set_a == TritSet({ Trit::True, Trit::Unknown, Trit::False }));
        REQUIRE(~(~set_a) == set_a);

        REQUIRE(~TritSet({ Trit::False, Trit::False, Trit::False, Trit::False, Trit::False, Trit::False, Trit::False })
                == TritSet({ Trit::True, Trit::True, Trit::True, Trit::True, Trit::True, Trit::True, Trit::True }));
    }
}


TEST_CASE("test swap method")
{
    TritSet set_a = { Trit::True, Trit::Unknown, Trit::False };
    TritSet set_b = { Trit::Unknown, Trit::True, Trit::Unknown };

    REQUIRE(set_b.length() == 2);

    auto copy_a = set_a;
    auto copy_b = set_b;

    set_a.swap(set_a);

    REQUIRE(set_a == copy_a);

    set_a.swap(set_b);

    REQUIRE(set_a == copy_b);
    REQUIRE(set_b == copy_a);

    set_b.swap(set_a);

    REQUIRE(set_a == copy_a);
    REQUIRE(set_b == copy_b);
}


TEST_CASE("test TritSet comparison and assignment operators")
{
    TritSet const set_a = {Trit::True, Trit::False, Trit::False, Trit::Unknown, Trit::True};
    TritSet set_b(200);

    std::copy(set_a.begin(), set_a.end(), set_b.begin());

    REQUIRE(set_a == set_b);

    set_b.trim(0);

    REQUIRE(set_a != set_b);

    set_b = set_a;

    REQUIRE(set_a == set_b);

    TritSet const set_c = {Trit::True, Trit::False, Trit::Unknown, Trit::False, Trit::True};

    REQUIRE(set_a != set_c);
    REQUIRE(set_b != set_c);
}


TEST_CASE("test automatic memory management and element assignment")
{
    WHEN("empty set is created")
    {
        TritSet set;

        THEN("set capacity equals zero")
        {
            REQUIRE(set.capacity() == 0u);
        }

        THEN("set length equals zero")
        {
            REQUIRE(set.length() == 0u);
        }

        THEN("no memory is allocated")
        {
            REQUIRE(set.get_data_array_size() == 0u);
            REQUIRE(set.get_data_pointer() == nullptr);
        }
    }

    GIVEN("a set with positive initial capacity")
    {
        TritSet set(10);

        auto p = set.get_data_pointer();
        auto capacity = set.capacity();

        REQUIRE(capacity >= 10);

        WHEN("assigning a value to an element")
        {
            WHEN("element position is within set's capacity")
            {
                REQUIRE(set.capacity() > 5);

                set[5] = Trit::True;

                THEN("no memory reallocation happens")
                {
                    REQUIRE(set.get_data_pointer() == p);
                    REQUIRE(set.capacity() == capacity);
                    REQUIRE(set.length() == 6);
                }
            }

            WHEN("element position exceeds set's capacity")
            {
                WHEN("assigned value is Unknown")
                {
                    set[1000] = Trit::Unknown;

                    THEN("no memory reallocation happens")
                    {
                        REQUIRE(set.get_data_pointer() == p);
                        REQUIRE(set.capacity() == capacity);
                        REQUIRE(set.length() == 0);
                    }
                }

                WHEN("assigned value is not Unknown")
                {
                    set[1000] = Trit::True;
                    
                    set[0] = Trit::True;

                    for (size_t i = 1; i < 10; ++i)
                    {
                        set[i] = set[i - 1];
                    }

                    set[555] = Trit::True;

                    THEN("memory is reallocated")
                    {
                        REQUIRE(set.get_data_pointer() != p);
                        REQUIRE(set.capacity() > capacity);
                        REQUIRE(set.capacity() >= 1000);
                        REQUIRE(set.length() == 1001);
                    }

                    WHEN("Unknown value is assigned to last known trit")
                    {
                        set[1000] = Trit::Unknown;

                        THEN("length changes accordingly")
                        {
                            REQUIRE(set.length() == 556);
                        }

                        set[555] = Trit::Unknown;

                        THEN("length changes accordingly")
                        {
                            REQUIRE(set.length() == 10);
                        }
                    }
                }
            }
        }
    }
}


TEST_CASE("test cardinality")
{
    TritSet set = {
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown,
            Trit::True, Trit::True, Trit::Unknown, Trit::True, Trit::False, Trit::Unknown
    };

    REQUIRE(set.cardinality(Trit::True) == 21);
    REQUIRE(set.cardinality(Trit::False) == 7);
    REQUIRE(set.cardinality(Trit::Unknown) == 13);

    auto cardinality = set.cardinality();

    REQUIRE(cardinality[Trit::True] == 21);
    REQUIRE(cardinality[Trit::False] == 7);
    REQUIRE(cardinality[Trit::Unknown] == 13);

    SECTION("empty set")
    {
        set.trim(0);

        REQUIRE(set.cardinality(Trit::True) == 0);
        REQUIRE(set.cardinality(Trit::False) == 0);
        REQUIRE(set.cardinality(Trit::Unknown) == 0);

        cardinality = set.cardinality();

        REQUIRE(cardinality[Trit::True] == 0);
        REQUIRE(cardinality[Trit::False] == 0);
        REQUIRE(cardinality[Trit::Unknown] == 0);
    }
}


TEST_CASE("test operator>>=")
{
    SECTION("empty set")
    {
        TritSet set;

        SECTION("zero offset")
        {
            TritSet copy = set;

            set >>= 0;

            REQUIRE(set == copy);
        }

        SECTION("positive offset")
        {
            SECTION("offset is divisible by number of trits in one block")
            {
                TritSet copy = set;

                set >>= sizeof(unsigned int) * 4 * 3;

                REQUIRE(set == copy);
            }
            SECTION("offset is not divisible by number of trits in one block")
            {
                TritSet copy = set;

                set >>= sizeof(unsigned int) * 4 * 3 + 7;

                REQUIRE(set == copy);
            }
        }
    }

    SECTION("non-empty set")
    {
        SECTION("zero offset")
        {
            TritSet set = {
                    Trit::True,
                    Trit::True,
                    Trit::True,
                    Trit::True,
                    Trit::True,
                    Trit::True,
                    Trit::True,
                    Trit::True,
                    Trit::True,
                    Trit::True,
                    Trit::True
            };

            auto copy = set;

            set >>= 0;

            REQUIRE(copy == set);
        }

        SECTION("positive offset")
        {
            SECTION("offset is divisible by number of trits in one block")
            {
                TritSet set = {
                        Trit::True,
                        Trit::Unknown,
                        Trit::False
                };

                TritSet result;

                result[sizeof(unsigned int) * 4 * 2 + 0] = Trit::True;
                result[sizeof(unsigned int) * 4 * 2 + 1] = Trit::Unknown;
                result[sizeof(unsigned int) * 4 * 2 + 2] = Trit::False;

                set >>= sizeof(unsigned int) * 4 * 2;

                REQUIRE(set == result);
            }

            SECTION("offset is not divisible by number of trits in one block")
            {
                TritSet set = {
                        Trit::True,
                        Trit::Unknown,
                        Trit::False
                };

                TritSet result;

                result[sizeof(unsigned int) * 4 * 2 + 9 + 0] = Trit::True;
                result[sizeof(unsigned int) * 4 * 2 + 9 + 1] = Trit::Unknown;
                result[sizeof(unsigned int) * 4 * 2 + 9 + 2] = Trit::False;

                set >>= sizeof(unsigned int) * 4 * 2 + 9;

                REQUIRE(set == result);
            }
        }

        SECTION(">>= is associative")
        {
            TritSet s = { Trit::False, Trit::True };

            auto initial_string = s.to_string();

            s >>= 3;

            REQUIRE(s == TritSet({ Trit::Unknown, Trit::Unknown, Trit::Unknown, Trit::False, Trit::True }));

            s >>= 11;

            REQUIRE(s == TritSet({
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown,
                Trit::False, Trit::True }));

            s >>= 1;

            REQUIRE(s == TritSet({
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::Unknown, Trit::Unknown, Trit::Unknown,
                Trit::False, Trit::True }));

            REQUIRE((TritSet({ Trit::False, Trit::True }) >>= (3 + 11 + 1)) == s);
        }

        SECTION("one more general test")
        {
            TritSet set;

            for (size_t idx = 0; idx < sizeof(unsigned int) * 4 * 2; ++idx)
            {
                set[idx] = Trit::True;
            }

            REQUIRE(set.length() == sizeof(unsigned int) * 4 * 2);
            REQUIRE(set.capacity() == set.length());

            set >>= 4;

            REQUIRE(set.length() == sizeof(unsigned int) * 4 * 2 + 4);
            REQUIRE(set.capacity() >= set.length());

            TritSet result;

            for (size_t idx = 4; idx < sizeof(unsigned int) * 4 * 2 + 4; ++idx)
            {
                result[idx] = Trit::True;
            }

            REQUIRE(set == result);
        }
    }
}


TEST_CASE("test operator<<=")
{
    SECTION("empty set")
    {
        TritSet set;

        SECTION("zero offset")
        {
            TritSet copy = set;
            set <<= 0;

            REQUIRE(set == copy);
        }

        SECTION("positive offset")
        {
            SECTION("offset is divisible by number of trits in one block")
            {
                TritSet copy = set;
                set <<= sizeof(unsigned int) * 4 * 3;

                REQUIRE(set == copy);
            }
            SECTION("offset is not divisible by number of trits in one block")
            {
                TritSet copy = set;
                set <<= sizeof(unsigned int) * 4 * 3 + 7;

                REQUIRE(set == copy);
            }
        }
    }

    SECTION("non-empty set")
    {
        SECTION("zero offset")
        {
            TritSet set = {
                    Trit::True,
                    Trit::True,
                    Trit::True,
                    Trit::True,
                    Trit::True,
                    Trit::True,
                    Trit::True,
                    Trit::True,
                    Trit::True,
                    Trit::True,
                    Trit::True
            };

            auto copy = set;

            set <<= 0;

            REQUIRE(copy == set);
        }

        SECTION("positive offset")
        {
            SECTION("offset is divisible by number of trits in one block")
            {
                TritSet set;

                set[sizeof(unsigned int) * 4 * 2 + 10] = Trit::True;
                set[sizeof(unsigned int) * 4 * 2 + 11] = Trit::Unknown;
                set[sizeof(unsigned int) * 4 * 2 + 12] = Trit::False;

                set <<= sizeof(unsigned int) * 4 * 2;

                TritSet result;

                result[10] = Trit::True;
                result[11] = Trit::Unknown;
                result[12] = Trit::False;

                REQUIRE(set == result);
            }

            SECTION("offset is not divisible by number of trits in one block")
            {
                SECTION("offset is less than block capacity")
                {
                    TritSet set = {
                            Trit::True,
                            Trit::True,
                            Trit::True,
                            Trit::True,
                            Trit::True,
                            Trit::True,
                            Trit::True
                    };

                    set <<= 3;

                    REQUIRE(set == TritSet({ Trit::True, Trit::True, Trit::True, Trit::True }));
                }

                SECTION("offset is greater than block capacity")
                {
                    TritSet set;

                    set[sizeof(unsigned int) * 4 * 2 + 10] = Trit::True;
                    set[sizeof(unsigned int) * 4 * 2 + 11] = Trit::Unknown;
                    set[sizeof(unsigned int) * 4 * 2 + 12] = Trit::False;

                    set <<= sizeof(unsigned int) * 4 * 2 + 9;

                    TritSet result;

                    result[1] = Trit::True;
                    result[2] = Trit::Unknown;
                    result[3] = Trit::False;

                    REQUIRE(set == result);
                }
            }

            SECTION("offset is bigger than length")
            {
                SECTION("empty set")
                {
                    TritSet set;

                    set <<= 42;

                    REQUIRE(set.empty());
                }

                SECTION("not so empty set")
                {
                    TritSet set(200);

                    set[199] = Trit::False;

                    for (auto & trit : set)
                    {
                        trit = Trit::True;
                    }

                    set <<= 420;

                    REQUIRE(set.empty());
                }
            }
        }
    }

    SECTION("some other cases")
    {
        SECTION("combination of left and right shifts")
        {
            TritSet set = { Trit::False, Trit::Unknown, Trit::True };

            auto copy = set;

            size_t initial_d = set.length();

            set >>= 271;

            set <<= 169;

            set >>= 42;

            (set >>= 1) <<= (271 - 169 + 42 + 1);

            REQUIRE(set.length() == initial_d);

            REQUIRE(copy == set);
        }

        SECTION("check if unused array blocks are filled with Trit::Unknown values")
        {
            TritSet set(1000);

            for (size_t i = 0; i < 1000; ++i)
            {
                set[i] = static_cast<Trit>(i % 3);
            }

            set <<= 995;

            REQUIRE(set == TritSet({ Trit::True, Trit::False, Trit::Unknown, Trit::True, Trit::False }));

            bool ok = true;

            TritSet unknown(sizeof(unsigned int) * 4);

            size_t init_blocks = ((set.length() * 2) + sizeof(unsigned int) * 8 - 1) / 8 / sizeof(unsigned int);

            auto end = set.get_data_pointer() + set.get_data_array_size();

            for (auto p = set.get_data_pointer() + init_blocks; p != end; ++p)
            {
                ok = *p == *unknown.get_data_pointer();
            }

            set[999] = Trit::True;

            REQUIRE(ok);
        }
    }
}


TEST_CASE("test operator+= and operator+")
{
    SECTION("empty sets")
    {
        REQUIRE((TritSet() + TritSet()).empty());
        REQUIRE((TritSet({ Trit::True }) + TritSet()) == TritSet({ Trit::True }));
        REQUIRE((TritSet() + TritSet({ Trit::True })) == TritSet({ Trit::True }));
    }

    TritSet set_a = { Trit::True, Trit::False, Trit::False };
    TritSet set_b = { Trit::Unknown, Trit::Unknown, Trit::True };

    set_a += set_b;

    REQUIRE(set_a == TritSet({ Trit::True, Trit::False, Trit::False, Trit::Unknown, Trit::Unknown, Trit::True }));

    TritSet set_c = set_a + set_b;

    REQUIRE(set_c == TritSet({
        Trit::True, Trit::False, Trit::False,
        Trit::Unknown, Trit::Unknown, Trit::True,
        Trit::Unknown, Trit::Unknown, Trit::True
    }));
}
