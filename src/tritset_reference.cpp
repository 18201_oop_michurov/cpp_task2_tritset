#include "tritset.hpp"


TritSet::reference::reference(
        size_t index,
        TritSet & trit_set)
        :
        index(index),
        trit_set(trit_set)
{}


TritSet::reference & TritSet::reference::operator=(
        Trit const trit)
{
    this->trit_set.set(this->index, trit);

    return *this;
}


TritSet::reference & TritSet::reference::operator=(
        TritSet::reference const & other)
{
    this->trit_set.set(this->index, other);

    return *this;
}


TritSet::reference & TritSet::reference::operator|=(
        Trit const trit)
{
    this->trit_set.set(this->index, *this | trit);

    return *this;
}


TritSet::reference & TritSet::reference::operator&=(
        Trit const trit)
{
    this->trit_set.set(this->index, *this & trit);

    return *this;
}


TritSet::reference::operator Trit() const
{
    return this->trit_set.get(this->index);
}
