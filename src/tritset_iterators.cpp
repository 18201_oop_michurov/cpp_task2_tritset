#include "tritset.hpp"


/* ------------------------------------------------------------------------------------------------------------------ */
/*                                             TritSet::const_iterator                                                */
/* ------------------------------------------------------------------------------------------------------------------ */


TritSet::const_iterator::const_iterator(
        size_t index,
        TritSet const & trit_set)
        :
        index(index),
        trit_set(trit_set)
{}


TritSet::const_iterator::const_iterator(
        TritSet::const_iterator const & other)
        :
        index(other.index),
        trit_set(other.trit_set)
{}


bool TritSet::const_iterator::operator!=(
        const_iterator const & other)
{
    return this->index != other.index or &this->trit_set != &other.trit_set;
}


Trit TritSet::const_iterator::operator*() const
{
    return this->trit_set.get(this->index);
}


TritSet::const_iterator & TritSet::const_iterator::operator++()
{
    this->index++;

    return *this;
}


/* ------------------------------------------------------------------------------------------------------------------ */
/*                                                TritSet::iterator                                                   */
/* ------------------------------------------------------------------------------------------------------------------ */


TritSet::iterator::iterator(
        size_t index,
        TritSet & trit_set)
        :
        element(new TritSet::reference(index, trit_set))
{}


TritSet::iterator::iterator(
        TritSet::iterator const & other)
        :
        element(new TritSet::reference(*other.element))
{}


bool TritSet::iterator::operator!=(
        iterator const & other)
{
    return this->element->index != other.element->index or &this->element->trit_set != &other.element->trit_set;
}


TritSet::reference & TritSet::iterator::operator*() const
{
    return *this->element;
}


TritSet::iterator & TritSet::iterator::operator++()
{
    this->element->index++;

    return *this;
}


TritSet::iterator::~iterator()
{
    delete element;
    this->element = nullptr;
}
