#ifndef CPP_TASK2_TRITSET_TRIT_HPP
#define CPP_TASK2_TRITSET_TRIT_HPP


#include <iostream>


enum class Trit : unsigned int
{
    False   = 0u,
    Unknown = 1u,
    True    = 2u
};


Trit & operator&=(
        Trit & t1,
        Trit t2);


Trit & operator|=(
        Trit & t1,
        Trit t2);


Trit operator~(
        Trit trit);


Trit operator&(
        Trit t1,
        Trit t2);


Trit operator|(
        Trit t1,
        Trit t2);


std::ostream & operator<<(
        std::ostream & os,
        Trit trit);


#endif //CPP_TASK2_TRITSET_TRIT_HPP
