#include "trit.hpp"


Trit & operator&=(
        Trit & t1,
        Trit const t2)
{
    return t1 = t1 < t2 ? t1 : t2;
}


Trit & operator|=(
        Trit & t1,
        Trit const t2)
{
    return t1 = t1 > t2 ? t1 : t2;
}


Trit operator~(
        Trit const trit)
{
    return static_cast<Trit>(2u - static_cast<unsigned int>(trit));
}


Trit operator&(
        Trit t1,
        Trit const t2)
{
    return t1 &= t2;
}


Trit operator|(
        Trit t1,
        Trit const t2)
{
    return t1 |= t2;
}
