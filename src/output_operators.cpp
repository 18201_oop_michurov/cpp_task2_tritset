#include "tritset.hpp"
#include "trit.hpp"


std::ostream & operator<<(
        std::ostream & os,
        TritSet::reference const & element)
{
    char c[] = {'0', '?', '1'};
    os << c[static_cast<int>(element.trit_set.get(element.index))];

    return os;
}


std::ostream & operator<<(
        std::ostream & os,
        Trit const trit)
{
    char c[] = {'0', '?', '1'};
    os << c[static_cast<int>(trit)];

    return os;
}


std::ostream & operator<<(
        std::ostream & os,
        TritSet const & set)
{
    os << set.to_string();

    return os;
}
