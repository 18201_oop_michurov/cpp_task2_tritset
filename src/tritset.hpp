#ifndef CPP_TASK2_TRITSET_TRITSET_HPP
#define CPP_TASK2_TRITSET_TRITSET_HPP


#include <cstdlib>
#include <string>
#include <iostream>
#include <unordered_map>


#include "trit.hpp"


class TritSet
{
private:
    class reference;

    constexpr static unsigned int unknown_block()
    {
        auto filler_value = static_cast<unsigned int>(Trit::Unknown);

        for (size_t i = 2; i < sizeof(unsigned int) * 8; i += 2)
        {
            filler_value |= (static_cast<unsigned int>(Trit::Unknown) << i);
        }

        return filler_value;
    }

    constexpr static unsigned int true_block()
    {
        auto filler_value = static_cast<unsigned int>(Trit::True);

        for (size_t i = 2; i < sizeof(unsigned int) * 8; i += 2)
        {
            filler_value |= (static_cast<unsigned int>(Trit::True) << i);
        }

        return filler_value;
    }

    constexpr static unsigned int false_block()
    {
        return 0u;
    }

    static unsigned int tritwise_and(
            unsigned int op1,
            unsigned int op2);

    static unsigned int tritwise_or(
            unsigned int op1,
            unsigned int op2);

    static unsigned int tritwise_not(
            unsigned int op1);

    void count_length(size_t start_index);

    void resize(size_t new_array_size);

    void release_resources() noexcept;

public:
    class iterator;
    class const_iterator;

    iterator begin();
    iterator end();

    const_iterator begin() const;
    const_iterator end() const;

    TritSet();

    TritSet(std::initializer_list<Trit> const & list);

    TritSet(TritSet && other) noexcept;

    TritSet(TritSet const & other);

    explicit TritSet(size_t capacity, Trit value = Trit::Unknown);

    ~TritSet();

    Trit get(size_t index) const;

    void set(
            size_t index,
            Trit trit);

    void swap(TritSet & other) noexcept;

    void shrink();

    void trim(size_t last_index);

    std::string to_string() const;

    size_t capacity() const;

    bool empty() const;

    size_t get_data_array_size() const;

    unsigned int const * get_data_pointer() const;

    size_t length() const;

    size_t cardinality(Trit value) const;

    std::unordered_map<Trit, size_t>  cardinality() const;

    TritSet & operator>>=(size_t shift);

    TritSet &  operator<<=(size_t shift);

    reference operator[](size_t index);

    Trit operator[](size_t index) const;

    TritSet & operator=(TritSet const & other);

    TritSet & operator=(TritSet && other) noexcept;

    TritSet & operator&=(TritSet const & other);

    TritSet & operator|=(TritSet const & other);

    TritSet & operator+=(TritSet other);

    friend TritSet operator~(
            TritSet const & set);

    friend std::ostream & operator<<(
            std::ostream & os,
            TritSet::reference const & element);

    friend bool operator!=(
            TritSet const & set1,
            TritSet const & set2);

    friend bool operator==(
            TritSet const & set1,
            TritSet const & set2);

private:
    size_t array_size;
    size_t logical_length;
    size_t initial_array_size;

    unsigned int * data;
};


class TritSet::reference
{
private:
    friend TritSet;
    friend TritSet::iterator;

    reference(
            size_t index,
            TritSet & trit_set);

public:
    reference(
            reference const & other) = default;

    reference & operator=(
            Trit trit);

    reference & operator=(
            reference const & other);

    reference & operator|=(
            Trit trit);

    reference & operator&=(
            Trit trit);

    friend std::ostream & operator<<(
            std::ostream & os,
            TritSet::reference const & element);

    operator Trit() const;

private:
    size_t index;
    TritSet & trit_set;
};


class TritSet::iterator : public std::iterator<std::input_iterator_tag, TritSet::reference>
{
private:
    friend TritSet;

    iterator(
            size_t index,
            TritSet & trit_set);

public:
    iterator(
            iterator const & other);

    bool operator!=(
            iterator const & other);

    TritSet::reference & operator*() const;

    iterator & operator++();

    ~iterator();

private:
    TritSet::reference * element;
};


class TritSet::const_iterator : public std::iterator<std::input_iterator_tag, Trit const>
{
private:
    friend TritSet;

    const_iterator(
            size_t index,
            TritSet const & trit_set);

public:
    const_iterator(
            const_iterator const & other);

    bool operator!=(
            const_iterator const & other);

    Trit operator*() const;

    const_iterator & operator++();

private:
    size_t index;
    TritSet const & trit_set;
};


TritSet operator+(
        TritSet set1,
        TritSet const & set2);


TritSet operator&(
        TritSet set1,
        TritSet const & set2);


TritSet operator|(
        TritSet set1,
        TritSet const & set2);


TritSet operator~(
        TritSet const & set);


TritSet operator>>(
        TritSet set,
        size_t offset);


TritSet operator<<(
        TritSet set,
        size_t offset);


bool operator!=(
        TritSet const & set1,
        TritSet const & set2);


bool operator==(
        TritSet const & set1,
        TritSet const & set2);


std::ostream & operator<<(
        std::ostream & os,
        TritSet::reference const & element);


std::ostream & operator<<(
        std::ostream & os,
        TritSet const & set);


#endif //CPP_TASK2_TRITSET_TRITSET_HPP
