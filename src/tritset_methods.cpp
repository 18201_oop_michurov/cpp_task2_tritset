#include <algorithm>
#include <unordered_map>


#include "tritset.hpp"
#define TEST_OUTPUT_MOVE_ASSIGNMENT
#define TEST_OUTPUT_MOVE_CONSTRUCTION


TritSet::iterator TritSet::begin()
{
    return TritSet::iterator(0, *this);
}


TritSet::iterator TritSet::end()
{
    return TritSet::iterator(this->logical_length, *this);
}


TritSet::const_iterator TritSet::begin() const
{
    return TritSet::const_iterator(0, *this);
}


TritSet::const_iterator TritSet::end() const
{
    return TritSet::const_iterator(this->logical_length, *this);
}


TritSet::TritSet(
        size_t capacity,
        Trit value)
        :
        array_size((capacity  + sizeof(unsigned int) * 4 - 1) / (sizeof(unsigned int) * 4)),
        logical_length(value == Trit::Unknown ? 0 : capacity),
        initial_array_size(array_size),
        data(array_size ? new unsigned int[array_size] : nullptr)

{
    unsigned int filler_value =
            value == Trit::True ? TritSet::true_block() :
            value == Trit::False ? TritSet::false_block() :TritSet::unknown_block();

    std::fill(this->data, this->data + this->array_size, filler_value);

    size_t bits_in_last_block = capacity % (sizeof(unsigned int) * 4) * 2;

    if (this->data != nullptr and value != Trit::Unknown and bits_in_last_block != 0)
    {
        this->data[this->array_size - 1] &= ~0u >> (8 * sizeof(unsigned int) - bits_in_last_block);
        this->data[this->array_size - 1] |= TritSet::unknown_block() << bits_in_last_block;
    }
}


TritSet::TritSet(TritSet const & other)
        :
        array_size(other.array_size),
        logical_length(other.logical_length),
        initial_array_size(other.initial_array_size),
        data(array_size ? new unsigned int[array_size] : nullptr)
{
    std::copy(other.data, other.data + other.array_size, this->data);
}


TritSet::TritSet(TritSet && other) noexcept
        :
        array_size(other.array_size),
        logical_length(other.logical_length),
        initial_array_size(other.initial_array_size),
        data(other.data)
{
    other.release_resources();
}


TritSet::TritSet()
        :
        array_size(0),
        logical_length(0),
        initial_array_size(0),
        data(nullptr)
{}


TritSet::TritSet(
        std::initializer_list<Trit> const & list)
        :
        array_size((list.size()  + sizeof(unsigned int) * 4 - 1) / (sizeof(unsigned int) * 4)),
        logical_length(0),
        initial_array_size(array_size),
        data(array_size ? new unsigned int[array_size] : nullptr)
{
    size_t index = 0;

    std::fill(this->data, this->data + this->array_size, TritSet::unknown_block());

    for (auto const trit : list)
    {
        if (trit != Trit::Unknown)
        {
            this->set(index, trit);
            this->logical_length = index + 1;
        }

        ++index;
    }

    if (list.size() > 0 and this->logical_length == 0)
    {
        delete[] this->data;

        this->data = nullptr;
        this->array_size = 0;
        this->initial_array_size = 0;

        return;
    }

    if (this->logical_length < list.size())
    {
        this->initial_array_size = 0;
        this->shrink();
        this->initial_array_size = array_size;
    }
}


TritSet::~TritSet()
{
    delete[] this->data;

    this->release_resources();
}


Trit TritSet::get(
        size_t index) const
{
    if (index >= this->logical_length)
    {
        return Trit::Unknown;
    }

    size_t data_offset = index / (4 * sizeof(unsigned int));

    size_t trit_offset_in_bits = index % (4 *sizeof(unsigned int)) * 2;

    return static_cast<Trit>((this->data[data_offset] >> trit_offset_in_bits) & 3u);
}


void TritSet::set(
        size_t index,
        Trit trit)
{
    size_t data_index = index / (4 * sizeof(unsigned int));

    if (data_index >= this->array_size)
    {
        if (trit == Trit::Unknown)
        {
            return;
        }
        else
        {
            this->resize(data_index + 1);
        }
    }

    size_t trit_offset_in_bits = index % (4 *sizeof(unsigned int)) * 2;

    this->data[data_index] &= ~(3u << trit_offset_in_bits);
    this->data[data_index] |= static_cast<unsigned int>(trit) << trit_offset_in_bits;

    if (index >= this->logical_length and trit != Trit::Unknown)
    {
        this->logical_length = index + 1;
    }
    else if (index == this->logical_length - 1 and trit == Trit::Unknown)
    {
        this->count_length(data_index);
    }
}


size_t TritSet::capacity() const
{
    return this->array_size * 4 * sizeof(unsigned int);
}


bool TritSet::empty() const
{
    return this->logical_length == 0;
}


size_t TritSet::get_data_array_size() const
{
    return this->array_size;
}


unsigned int const * TritSet::get_data_pointer() const
{
    return this->data;
}


size_t TritSet::length() const
{
    return this->logical_length;
}


size_t TritSet::cardinality(Trit value) const
{
    size_t cardinality = 0;
    size_t last_init_block_index = this->logical_length / (sizeof(unsigned int) * 4);
    size_t bits_in_last_block = this->logical_length % (sizeof(unsigned int) * 4) * 2;

    for (size_t k = 0; k < last_init_block_index; ++k)
    {
        for (size_t offset = 0; offset < sizeof(unsigned int) * 8; offset += 2)
        {
            cardinality += static_cast<Trit>((this->data[k] >> offset) & 3u) == value;
        }
    }

    for (size_t offset = 0; offset < bits_in_last_block; offset += 2)
    {
        cardinality += static_cast<Trit>((this->data[last_init_block_index] >> offset) & 3u) == value;
    }

    return cardinality;
}


std::unordered_map<Trit, size_t>  TritSet::cardinality() const
{
    std::unordered_map<Trit, size_t> cardinality = {
            {Trit::False, 0},
            {Trit::Unknown, 0},
            {Trit::True, 0}
    };

    size_t last_init_block_index = this->logical_length / (sizeof(unsigned int) * 4);
    size_t bits_in_last_block = this->logical_length % (sizeof(unsigned int) * 4) * 2;

    for (size_t k = 0; k < last_init_block_index; ++k)
    {
        for (size_t offset = 0; offset < sizeof(unsigned int) * 8; offset += 2)
        {
            cardinality[static_cast<Trit>((this->data[k] >> offset) & 3u)]++;
        }
    }

    for (size_t offset = 0; offset < bits_in_last_block; offset += 2)
    {
        cardinality[static_cast<Trit>((this->data[last_init_block_index] >> offset) & 3u)]++;
    }

    return cardinality;
}


std::string TritSet::to_string() const
{
    std::string repr(this->logical_length, '?');

    char char_set[] = {'0', '?', '1'};

    size_t char_index = 0;
    size_t last_used_block = this->logical_length / (sizeof(unsigned int) * 4);
    size_t bits_in_last_block = this->logical_length % (sizeof(unsigned int) * 4) * 2;

    for (size_t data_index = 0; data_index < last_used_block; ++data_index)
    {
        for (size_t offset = 0; offset < sizeof(unsigned int) * 8; offset += 2)
        {
            repr[char_index++] = char_set[(this->data[data_index] >> offset) & 0x03u];
        }
    }

    for (size_t offset = 0; offset < bits_in_last_block; offset += 2)
    {
        repr[char_index++] = char_set[(this->data[last_used_block] >> offset) & 0x03u];
    }

    return repr;
}


void TritSet::swap(
        TritSet & other) noexcept
{
    std::swap(this->array_size, other.array_size);
    std::swap(this->initial_array_size, other.initial_array_size);
    std::swap(this->logical_length, other.logical_length);
    std::swap(this->data, other.data);
}


void TritSet::shrink()
{
    size_t init_length = (this->logical_length + sizeof(unsigned int) * 4 - 1) / (4 * sizeof(unsigned int));
    size_t new_size = init_length > this->initial_array_size ? init_length : this->initial_array_size;

    this->resize(new_size);
}


void TritSet::trim(
        size_t last_index)
{
    if (last_index >= this->logical_length)
    {
        return;
    }

    size_t last_block_index = last_index / (4 * sizeof(unsigned int));
    size_t trit_offset_in_bits = last_index % (4 *sizeof(unsigned int)) * 2;

    size_t init_blocks = (this->logical_length + sizeof(unsigned int) * 4 - 1) / (4 * sizeof(unsigned int));

    this->data[last_block_index] &= trit_offset_in_bits ?
            ((~0u) >> (8 * sizeof(unsigned int) - trit_offset_in_bits)) : 0u;

    this->data[last_block_index] |= TritSet::unknown_block() << trit_offset_in_bits;

    std::fill(this->data + last_block_index + 1, this->data + init_blocks, TritSet::unknown_block());

    this->count_length(last_block_index);
}


unsigned int TritSet::tritwise_and(
        unsigned int op1,
        unsigned int op2)
{
    unsigned int result = 0;
    unsigned int t1;
    unsigned int t2;

    for (size_t index = 0; index < sizeof(unsigned int) * 8; index += 2)
    {
        t1 = op1 & (3u << index);
        t2 = op2 & (3u << index);

        result |= t1 < t2 ? t1 : t2;
    }

    return result;
}


unsigned int TritSet::tritwise_or(
        unsigned int op1,
        unsigned int op2)
{
    unsigned int result = 0;
    unsigned int t1;
    unsigned int t2;

    for (size_t index = 0; index < sizeof(unsigned int) * 8; index += 2)
    {
        t1 = op1 & (3u << index);
        t2 = op2 & (3u << index);

        result |= t1 > t2 ? t1 : t2;
    }

    return result;
}


unsigned int TritSet::tritwise_not(
        unsigned int op1)
{
    return TritSet::true_block() - op1;
}


void TritSet::release_resources() noexcept
{
    this->data = nullptr;
    this->array_size = 0;
    this->initial_array_size = 0;
    this->logical_length = 0;
}


void TritSet::resize(
        size_t new_array_size)
{
    if (new_array_size == this->array_size)
    {
        return;
    }

    if (new_array_size == 0)
    {
        delete [] this->data;

        this->data = nullptr;
        this->array_size = 0;
        this->logical_length = 0;

        return;
    }

    size_t copy_size = new_array_size < this->array_size ? new_array_size : this->array_size;

    auto * new_data = new unsigned int[new_array_size];

    if (new_array_size > copy_size)
    {
        std::fill(new_data + this->array_size, new_data + new_array_size, TritSet::unknown_block());
    }

    std::copy(this->data, this->data + copy_size, new_data);

    delete [] this->data;

    this->data = new_data;
    this->array_size = new_array_size;

    this->count_length(this->array_size - 1);
}


void TritSet::count_length(
        size_t start_index)
{
    if (this->data == nullptr)
    {
        this->logical_length = 0;
        return;
    }

    if (start_index >= this->array_size)
    {
        throw std::runtime_error("Object internal state has been corrupted");
    }

    long long data_offset;

    for (data_offset = static_cast<long long>(start_index);
         data_offset > 0 and this->data[data_offset] == TritSet::unknown_block(); --data_offset)
    {}

    for (int bit_index = sizeof(unsigned int) * 8 - 2; bit_index >= 0; bit_index -= 2)
    {
        if (static_cast<Trit>((this->data[data_offset] >> static_cast<unsigned int>(bit_index)) & 3u) != Trit::Unknown)
        {
            this->logical_length = static_cast<size_t>(data_offset) * sizeof(unsigned int) * 4
                                   + static_cast<size_t>(bit_index) / 2 + 1;

            return;
        }
    }

    this->logical_length = 0;
}
