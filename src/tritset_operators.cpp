#include <algorithm>


#include "tritset.hpp"
#define TEST_OUTPUT_MOVE_ASSIGNMENT
#define TEST_OUTPUT_MOVE_CONSTRUCTION


TritSet &  TritSet::operator>>=(
        size_t shift)
{
    if (this->logical_length == 0 or shift == 0)
    {
        return *this;
    }

    size_t blocks_to_shift = shift / (4 * sizeof(unsigned int));
    size_t bits_to_shift = shift % (4 * sizeof(unsigned int)) * 2;
    size_t init_blocks = (this->logical_length + sizeof(unsigned int) * 4 - 1) / (4 * sizeof(unsigned int));
    size_t new_size = (this->logical_length + shift + sizeof(unsigned int) * 4 - 1) / (4 * sizeof(unsigned int));

    if (new_size > this->array_size)
    {
        this->resize(new_size);
    }

    auto begin = blocks_to_shift ?
                 std::rotate(
                         this->data,
                         this->data + this->array_size - blocks_to_shift,
                         this->data + this->array_size)
                 :
                 this->data;

    size_t trits_in_last_block = this->logical_length % (4 * sizeof(unsigned int)) == 0 ?
            4 * sizeof(unsigned int) : this->logical_length % (4 * sizeof(unsigned int));

    size_t start_index = trits_in_last_block * 2 + bits_to_shift > 8 * sizeof(unsigned int) ? init_blocks : init_blocks - 1;

    this->logical_length += shift;

    if (bits_to_shift == 0)
    {
        return *this;
    }

    for (size_t i = start_index; i > 0; --i)
    {
        begin[i] <<= bits_to_shift;
        begin[i] |= begin[i - 1] >> (sizeof(unsigned int) * 8 - bits_to_shift);
    }

    *begin <<= bits_to_shift;
    *begin |= TritSet::unknown_block() >> (sizeof(unsigned int) * 8 - bits_to_shift);

    return *this;
}


TritSet &  TritSet::operator<<=(
        size_t shift)
{
    if (this->logical_length == 0 or shift == 0)
    {
        return *this;
    }

    if (shift >= this->logical_length)
    {
        std::fill(this->data, this->data + this->array_size, TritSet::unknown_block());
        this->logical_length = 0;

        return *this;
    }

    size_t block_shift = shift / (4 * sizeof(unsigned int));
    size_t bit_shift = shift % (4 * sizeof(unsigned int)) * 2;

    size_t init_blocks = (this->logical_length + sizeof(unsigned int) * 4 - 1) / (4 * sizeof(unsigned int));

    if (bit_shift != 0)
    {
        for (size_t i = 0; i + 1 < init_blocks; ++i)
        {
            this->data[i] >>= bit_shift;
            this->data[i] |= this->data[i + 1] << (sizeof(unsigned int) * 8 - bit_shift);
        }

        this->data[init_blocks - 1] >>= bit_shift;
        this->data[init_blocks - 1] |= TritSet::unknown_block() << (sizeof(unsigned int) * 8 - bit_shift);
    }

    auto begin = std::rotate(this->data, this->data + block_shift, this->data + this->array_size);

    std::fill(begin, this->data + this->array_size, TritSet::unknown_block());

    this->logical_length -= shift;

    return *this;
}


TritSet & TritSet::operator=(TritSet const & other)
{
    if (this != &other)
    {
        TritSet(other).swap(*this);
    }

    return *this;
}


TritSet & TritSet::operator=(TritSet && other) noexcept
{
    delete[] this->data;
    this->data = nullptr;

    this->swap(other);

    other.release_resources();

    return *this;
}


TritSet::reference TritSet::operator[](size_t index)
{
    return reference(index, *this);
}


Trit TritSet::operator[](size_t index) const
{
    return this->get(index);
}


bool operator!=(
        TritSet const & set1,
        TritSet const & set2)
{
    size_t init_blocks = (set1.logical_length + sizeof(unsigned int) * 4 - 1) / (4 * sizeof(unsigned int));

    return set1.logical_length != set2.logical_length or not std::equal(set1.data, set1.data + init_blocks, set2.data);
}


bool operator==(
        TritSet const & set1,
        TritSet const & set2)
{
    return not (set1 != set2);
}


TritSet & TritSet::operator+=(TritSet other)
{
    if (other.logical_length == 0)
    {
        return *this;
    }

    other >>= this->logical_length;

    size_t blocks_to_nullify = this->logical_length / (4 * sizeof(unsigned int));
    size_t bits_to_nullify = this->logical_length % (4 * sizeof(unsigned int)) * 2;

    std::fill(other.data, other.data + blocks_to_nullify, TritSet::false_block());
    other.data[blocks_to_nullify] &= ~0u << bits_to_nullify;

    return *this |= other;
}


TritSet & TritSet::operator&=(TritSet const & other)
{
    size_t new_size = this->array_size > other.array_size ? this->array_size : other.array_size;

    this->resize(new_size);

    TritSet const & lesser_set = this->array_size <= other.array_size ? *this : other;
    TritSet const & greater_set = this->array_size > other.array_size ? *this : other;

    std::transform(
            greater_set.data,
            greater_set.data + lesser_set.array_size,
            lesser_set.data,
            this->data,
            TritSet::tritwise_and);

    std::transform(
            greater_set.data + lesser_set.array_size,
            greater_set.data + greater_set.array_size,
            this->data + lesser_set.array_size,
            [](unsigned int block){ return TritSet::tritwise_and(block, TritSet::unknown_block()); });

    this->count_length(new_size - 1);

    return *this;
}


TritSet & TritSet::operator|=(TritSet const & other)
{
    size_t new_size = this->array_size > other.array_size ? this->array_size : other.array_size;

    this->resize(new_size);

    TritSet const & lesser_set = this->array_size <= other.array_size ? *this : other;
    TritSet const & greater_set = this->array_size > other.array_size ? *this : other;

    std::transform(
            greater_set.data,
            greater_set.data + lesser_set.array_size,
            lesser_set.data,
            this->data,
            TritSet::tritwise_or);

    std::transform(
            greater_set.data + lesser_set.array_size,
            greater_set.data + greater_set.array_size,
            this->data + lesser_set.array_size,
            [](unsigned int block){ return TritSet::tritwise_or(block, TritSet::unknown_block()); });

    this->count_length(new_size - 1);

    return *this;
}


TritSet operator+(
        TritSet set1,
        TritSet const & set2)
{
    return set1 += set2;
}


TritSet operator&(
        TritSet set1,
        TritSet const & set2)
{
    return set1 &= set2;
}


TritSet operator|(
        TritSet set1,
        TritSet const & set2)
{
    return set1 |= set2;
}


TritSet operator>>(
        TritSet set,
        size_t const offset)
{
    return set >>= offset;
}


TritSet operator<<(
        TritSet set,
        size_t const offset)
{
    return set <<= offset;
}


TritSet operator~(
        TritSet const & set)
{
    TritSet inverted_set(set);

    size_t init_blocks = (inverted_set.logical_length + sizeof(unsigned int) * 4 - 1) / (4 * sizeof(unsigned int));

    std::transform(inverted_set.data, inverted_set.data + init_blocks, inverted_set.data, TritSet::tritwise_not);

    return inverted_set;
}
